QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { multi(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(multi(3,4), 12, 'All positive numbers');
    assert.strictEqual(multi(4,-6), -24, 'Positive and negative numbers');
    assert.strictEqual(multi(-3,-8), 24, 'All are negative numbers');
});
